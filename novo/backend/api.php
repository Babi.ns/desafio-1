<?php
	header('Access-Control-Allow-Origin: *');
	require_once('Conectar.php');

	//$_POST['consulta']
	if (isset($_POST['consulta'])) {
		$con = new Conectar();
		$consult = $con->consulta('SELECT * FROM info');
		$con = null;
		echo json_encode(array('dado' => $consult));
	}else if (isset($_POST['consulta_1'])) {
		$con = new Conectar();
		$consult = $con->consulta('SELECT * FROM info WHERE id_user = '.$_POST['id']);
		$con = null;
		echo json_encode(array('dado' => $consult));
	}else if (isset($_POST['inserir'])) {
		$con = new Conectar();
		$con->modificar('INSERT INTO info(nome_user, telefone, email) VALUES(:nome_user, :telefone, :email)', array(
	      ':nome_user' => $_POST['nome'],
	      ':telefone' => $_POST['telefone'],
	      ':email' => $_POST['email']
	    ));
	    echo json_encode(array('ins' => 'Inserido com sucesso!'));
	    $con = null;
	}elseif (isset($_POST['alterar'])) {
		$con = new Conectar();
		$con->modificar('UPDATE info SET nome_user = :nome WHERE id_user = :id', array(
	      ':id'   => $_POST['id'],
	      ':nome' => $_POST['nome']
	    ));
	    echo json_encode(array('ins' => 'Alterado com sucesso!'));
	    $con = null;
	}elseif (isset($_POST['deletar'])) {
		$con = new Conectar();
		$con->modificar('DELETE FROM info WHERE id_user = :id', array(
	      ':id'   => $_POST['id'],
	    ));
	    echo json_encode(array('ins' => 'Deletado com sucesso!'));
	    $con = null;
	}

	//Função Consulta
	/*$con = new Conectar();
	$con->consulta('SELECT * FROM info');
	$con = null;*/

	//Função Inserir
	/*$con = new Conectar();
	$con->modificar('INSERT INTO info(nome_user, telefone, email) VALUES(:nome_user, :telefone, :email)', array(
      ':nome_user' => 'André',
      ':telefone' => 40506070,
      ':email' => 'andre@outlook.com'
    ));
    $con = null;*/
	
	//Função Alterar
	/*$id = 1;
	$nome = 'BB';
	$con = new Conectar();
	$con->modificar('UPDATE info SET nome_user = :nome WHERE id_user = :id', array(
      ':id'   => $id,
      ':nome' => $nome
    ));
    $con = null;*/

	//Função Deletar
	/*$id = 1;
	$con = new Conectar();
	$con->modificar('DELETE FROM info WHERE id_user = :id', array(
      ':id'   => $id,
    ));
    $con = null;*/
?>