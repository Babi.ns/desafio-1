<?php
	class Conectar{

		public function consulta($busca){
			try {
		    $conn = new PDO('pgsql:host=localhost;dbname=babi', 'postgres', '123456');
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    
		    $dbsel = $conn->prepare($busca);
		    $dbsel->execute();

		    $row = $dbsel->fetchAll();
		    return $row;

		  } catch(PDOException $e) {
		      echo 'ERROR: ' . $e->getMessage();
		  }
		}

		public function modificar($valor, $dados){
			$conn = new PDO('pgsql:host=localhost;dbname=babi', 'postgres', '123456');
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$dbins = $conn->prepare($valor);
		    $dbins->execute($dados);
		}
	}
?>